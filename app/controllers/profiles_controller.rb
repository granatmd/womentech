class ProfilesController < ApplicationController
  http_basic_authenticate_with name: ENV["LOGIN"], password: ENV["PASSWORD"], except: %i(suggest profiles_json grab)
  before_action :set_profile, only: %i(destroy update)

  def index
    @tags = Profile::TAGLIST.sort_by! { |c| c.downcase }.to_json
    if params[:tags].blank?
      @profiles = Profile.newest
    else
      @profiles = Profile.with_tags(params[:tags].split(',')).newest
    end
    respond_to do |format|
      format.html
      format.json { render json: @profiles.to_json, status: 200 }
    end
  end

  def create
    @profile = Profile.new(profile_params)
    @profile.update!(approved: true)
    @profile.save!
    render json: @profile.to_json, status: 201
  end

  def update
    @profile.update!(profile_params)
    render json: @profile.to_json, status: 201
  end

  def destroy
    @profile.destroy
    render json: @profile.to_json, status: 200
  end

  def grab
    url = profile_params[:url]
    if url.include?('linkedin')
      if session[:atoken].nil?
        render json: { statusText: "Authorize linkedin" }, status: 422
      else
        @response = LnkdnApi.grab(url, session[:atoken], session[:asecret])
        taglist = Profile.where(url: url).last.try(:taglist)
        taglist.present? ? @response['taglist'] = taglist : @response['taglist'] = []
        render json: @response.to_json, status: @response['status']
      end
    elsif url.include?('twitter')
      @response = TwitterApi.grab(url)
      taglist = Profile.where(url: url).last.try(:taglist)
      taglist.present? ? @response['taglist'] = taglist : @response['taglist'] = []
      render json: @response.to_json, status: @response['status']
    else
      render json: { statusText: 'error' }, status: 422
    end
  end

  def suggest
    @profile = Profile.new(profile_params)
    @profile.update(approved: false)
    if @profile.save
      render json: @profile.to_json, status: 201
    else
      render json: @profile.errors.to_json, status: 422
    end
  end

  def profiles_json
    if params[:tags].blank?
      @profiles = Profile.approved.random
    else
      @profiles = Profile.approved.with_tags(params[:tags].split(',')).random
    end
    render json: @profiles.to_json, status: 200
  end

  def approve
    @profile = Profile.find(params[:profile_id])
    @profile.update! approved: true
  end

private

  def set_profile
    @profile = Profile.find_by(id: params[:id])
  end

  def profile_params
    params.require(:profile).permit(:bio, :name, :alias, :url, :image, taglist: [])
  end
end
