class PagesController < ApplicationController
  def index
    @tags = Profile::TAGLIST.sort_by! { |c| c.downcase }
  end

  def suggest
    @tags = Profile::TAGLIST.sort_by! { |c| c.downcase }
  end

  def about;end

  def request_li_token
    request_token = LnkdnApi.request_auth_url
    session[:rtoken] = request_token.token
    session[:rsecret] = request_token.secret
    url = request_token.authorize_url
    redirect_to url
  end

  def oauth
    pin = params[:oauth_verifier]
    credentials = LnkdnApi.authorize(session[:rtoken], session[:rsecret], pin)
    session[:atoken] = credentials[:atoken]
    session[:asecret] = credentials[:asecret]
    redirect_to admin_path
  end
end
