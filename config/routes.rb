Rails.application.routes.draw do
  root "pages#index"

  get '/suggest', to: "pages#suggest", as: :suggest
  get '/about', to: "pages#about", as: :about

  post 'grab', to: "profiles#grab"

  post 'suggest', to: "profiles#suggest"

  get 'profiles/json', to: "profiles#profiles_json"

  scope '/admin' do
    get '/',  to: "profiles#index", as: :admin
    resources :profiles, only: %i(create update destroy) do
      post :approve, to: :approve
    end
  end

  get 'oauth/callback', to: 'pages#oauth'
  get 'oauth/request_li_token', to: "pages#request_li_token", as: :li_token
end
