module TwitterApi
  def self.grab(page_url)
    client = Twitter::REST::Client.new do |config|

      config.consumer_key        = ENV["YOUR_CONSUMER_KEY"]
      config.consumer_secret     = ENV["YOUR_CONSUMER_SECRET"]
      config.access_token        = ENV["YOUR_ACCESS_TOKEN"]
      config.access_token_secret = ENV["YOUR_ACCESS_SECRET"]
    end

    details  = Hash.new
    begin
      if page_url.include?('twitter')
        username = page_url.split('/').last
      else
        raise "error"
      end
      details['source'] = 'twitter'
      details['name']        = client.user(username).name
      details['alias'] = client.user(username).screen_name
      details['bio'] = client.user(username).description || ''
      details['url']         = client.user(username).url.to_s
      details['image']       = client.user(username).profile_image_url.to_s.gsub('_normal', '_400x400').gsub('http://', 'https://')
      details['status'] = 200

    rescue
      details['status'] = 422
      details['statusText'] = "URL it's not valid or user not exists. e.g. of valid url: https://twitter.com/redjotter or https://www.facebook.com/JohnDoe"
    end

    details['bio'] = Urlfy.url(details['bio']) unless details['bio'] == nil
    return details
  end
end
