import Vue from 'vue/dist/vue.esm'
import VueResource from 'vue-resource'
Vue.use(VueResource)

import VeeValidate from 'vee-validate';
Vue.use(VeeValidate);

import VueScrollTo from 'vue-scrollto'
Vue.use(VueScrollTo)

import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

import addform from '../components/addform'
import profiles from '../components/profiles'
import sidebar from '../components/aside'
import indexProfiles from '../components/index-profiles'
import suggest from '../components/suggest'
import ScrollTop from '../components/scroll-top'
import SearchBox from '../components/search-box'
import SearchForm from '../components/search-form'

document.addEventListener('turbolinks:load', () => {
  Vue.http.headers.common['X-CSRF-Token'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content')

  var elem = document.getElementById('app')
  var body = document.getElementsByTagName('body')[0]
  if (elem != null) {
    var app = new Vue({
      el: elem,
      data() {
        return {
          loaded: false,
          asideOpened: false,
          fixedForm: false,
          searchQuery: ''
        }
      },
      components: {
        addform,
        profiles,
        'index-profiles': indexProfiles,
        sidebar,
        suggest,
        ScrollTop,
        SearchBox,
        SearchForm
      },
      methods: {
        toggleFormShow(e) {
          if (e.target.localName != 'input' && e.target.className.match(/\b(form|hold)\b/) === null) {
            this.fixedForm = !this.fixedForm
          }
        },
        updateSearchQuery(e) {
          this.searchQuery = e
        },
        resetSearch(hideForm) {
          this.searchQuery = ''
          if (!!hideForm) {
            this.fixedForm = false
          }
          this.$forceUpdate()
        },
        toggleAside() {
          this.asideOpened = !this.asideOpened
          if (this.asideOpened) {
            body.classList.add("body-no-scroll");
          } else {
            body.classList.remove("body-no-scroll");
          }
        },
        closeAsideOnClick(e) {
          var asideArea = false
          for (var i = 0; i < e.path.length; i++) {
            var classList = e.path[i].classList
            if (!!classList && classList.value.includes('aside')) {
              return asideArea = true
            } else {
              asideArea = false
            }
          }
          if (!asideArea) {
            this.asideOpened = false
            body.classList.remove("body-no-scroll");
          }
        }
      },
      mounted() {
        this.$nextTick(function() {
          window.addEventListener('click', this.closeAsideOnClick);
        })
        this.loaded = true
      }
    })
  }
})
