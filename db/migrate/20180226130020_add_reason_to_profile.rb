class AddReasonToProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :profiles, :reason, :text
  end
end
