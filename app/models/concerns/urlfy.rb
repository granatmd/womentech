module Urlfy
  def self.url(str)
    str = str.gsub(/(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9]\.[^\s]{2,})/) { |s| "<a href='#{s}' target='_blank'>#{s}</a>" }
    str = str.gsub(/[\b#]+\w+/) { |s| "<a href='https://twitter.com/hashtag/#{s.split('#').last}' target='_blank'>#{s}</a>" }
    str = str.gsub(/[\b@]+\w+/) { |s| "<a href='https://twitter.com/#{s.split('@').last}' target='_blank'>#{s}</a>" }

    str
  end
end
