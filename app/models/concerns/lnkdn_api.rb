module LnkdnApi
  def self.grab(page_url, atoken, asecret)
    client = self.init_client
    client.authorize_from_access(atoken, asecret)

    id = client.profile(url: page_url, :fields => %w(id)).id
    info = client.profile(id: id, :fields => %w(summary public_profile_url  positions formatted-name phonetic-first-name picture-url))
    details  = Hash.new
    begin
      details['source'] = 'linkedin'
      details['name'] = info.formatted_name
      details['alias'] = info.formatted_name.gsub(' ', '')
      company = info.positions['all'].try('last')
      details['bio'] = company.try('title').to_s + ' at ' + company.try('company').try('name').to_s + "\r\n" + info.summary.to_s
      details['url'] = info.public_profile_url
      details['image'] = info.picture_url
      details['status'] = 200
    rescue
      details['status'] = 422
      details['statusText'] = "URL it's not valid or user not exists."
    end

    details['bio'] = Urlfy.url(details['bio']) unless details['bio'] == nil
    return details
  end

  def self.request_auth_url
    client = self.init_client
    request_token = client.request_token({ oauth_callback: ENV['LI_CALLBACK'] },
                                           :scope => "r_basicprofile r_emailaddress")
    request_token
  end

  def self.authorize(rtoken, rsecret, pin)
    client = self.init_client
    atoken, asecret = client.authorize_from_request(rtoken, rsecret, pin)
    credentials = { atoken: atoken, asecret: asecret }
    credentials
  end

  private

  def self.init_client
    client = LinkedIn::Client.new(ENV['LI_CONSUMER_KEY'], ENV['LI_CONSUMER_SECRET'])
  end
end
