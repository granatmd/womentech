class Profile < ApplicationRecord
  TAGLIST = 'Developers|Testers|Founders|Graphic designers|UI/UX Designer|Top Managers|Illustrators|Leads|Digital Marketers|Project managers|IT Recruiters|Writers / Bloggers|Speakers|Web Designers|Content Strategists|CEOs|Creative Directors'.split('|')

  scope :newest, -> { order('created_at desc') }
  scope :approved, -> { where(approved: true) }
  scope :with_tags, ->(tags) { where("taglist && ARRAY[?]::varchar[]", tags) }
  scope :random, -> { order("RANDOM()") }
  validates :reason, length: { maximum: 400 }

  validates :url, :name, uniqueness: { message: "This person is already added :)" }
end
