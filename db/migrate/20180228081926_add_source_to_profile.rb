class AddSourceToProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :profiles, :source, :string
  end
end
