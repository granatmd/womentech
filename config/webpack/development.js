const webpack = require('webpack')
const environment = require('./environment')

const config = environment.toWebpackConfig()

// config.plugins = (config.plugins || []).concat([
//   new webpack.EnvironmentPlugin({
//     ROOT_HOST: ''
//   })
// ])

config.devtool = 'cheap-eval-source-map'
module.exports = config
