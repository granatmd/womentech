class AddApprovedToProfile < ActiveRecord::Migration[5.1]
  def change
    add_column :profiles, :approved, :boolean
  end
end
