class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :alias
      t.text :bio
      t.string :image
      t.string :url
      t.string :taglist, array: true, default: []

      t.timestamps
    end
    add_index :profiles, :taglist
  end
end
