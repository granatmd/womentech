const webpack = require('webpack')
const environment = require('./environment')

const config = environment.toWebpackConfig()

// config.plugins = (config.plugins || []).concat([
//   new webpack.EnvironmentPlugin({
//     ROOT_HOST: ''
//   })
// ])

config.devtool = 'hidden-source-map'
module.exports = config
