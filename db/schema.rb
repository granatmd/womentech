# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180228081926) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "li_access_keys", force: :cascade do |t|
    t.string "key_1"
    t.string "key_2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string "name"
    t.string "alias"
    t.text "bio"
    t.string "image"
    t.string "url"
    t.string "taglist", default: [], array: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "approved"
    t.text "reason"
    t.string "source"
    t.index ["taglist"], name: "index_profiles_on_taglist"
  end

end
