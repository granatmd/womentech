module Api
  def self.grab(page_url)
    page_url.to_s.include?('twitter') ? TwitterApi.grab(page_url) : LnkdnApi.grab(page_url)
  end
end
